//
//  TenPinBowlingGame.swift
//  TenPinBowling-DEV-167
//
//  Created by DEV-167 on 20/05/19.
//  Copyright © 2019 DEV-167. All rights reserved.
//

import Foundation

struct TenPinBowlingGame {
    
    let MAXIMUM_FRAMES = 10
    
    struct Frame {
        
        enum FrameType {
            case last
            case normal
        }
        
        private let MAXIMUM_PINS_PER_ROLL = 10
        private(set) var type: FrameType
        var firstRollPinFalls: Int?
        var secondRollPinFalls: Int?
        // This will be used only for last frame as a bonus roll, sometimes
        var thirdRollPinFalls: Int?
        
        init(type: FrameType) { self.type = type }
        
        @discardableResult
        mutating func record(pinsFell: Int) -> Bool {
            if !isFirstRollCompleted() { firstRollPinFalls = pinsFell }
            else if !isSecondRollCompleted() { secondRollPinFalls = pinsFell }
            else if type == .last { thirdRollPinFalls = pinsFell }
            return true
        }
        
        func isFirstRollCompleted() -> Bool { return firstRollPinFalls != nil }
        func isSecondRollCompleted() -> Bool { return secondRollPinFalls != nil }
        
        func isFrameCompleted() -> Bool {
            // First check if first roll it strike
            if type == .last && (isStrike() || isSpare()) {
                // 2nd and 3rd rolls must be non-nil
                if secondRollPinFalls != nil, thirdRollPinFalls != nil { return true }
                else { return false }
            } else if firstRollPinFalls != nil, isStrike() { return true }
            return firstRollPinFalls != nil && secondRollPinFalls != nil
        }
        
        func isSpare() -> Bool {
            if let firstRollPinFalls = firstRollPinFalls, let secondRollPinFalls = secondRollPinFalls {
                return (firstRollPinFalls + secondRollPinFalls) == MAXIMUM_PINS_PER_ROLL
            }
            return false
        }
        
        func isStrike() -> Bool { return firstRollPinFalls == MAXIMUM_PINS_PER_ROLL }
    }
    
    private(set) var completedFrames = [Frame]()
    private(set) var currentFrame: Frame!
    
    var didEndGame: ((_ score: Int) -> Void)?
    var score: Int = 0
    
    init() { currentFrame = Frame(type: .normal) }
    
    @discardableResult
    mutating func record(pinsFell: Int) -> Bool {
        
        // Maxium frames have been recorded. No more recording rolls.
        if hasGameEnded() { return false }
        
        if currentFrame.record(pinsFell: pinsFell) {
            if currentFrame.isFrameCompleted() {
                completedFrames.append(currentFrame)
                
                // Maxium frames have been recorded. Notifying via closure.
                if hasGameEnded() { notifyScore() }
                else {
                    var type = Frame.FrameType.normal
                    if completedFrames.count + 1 == MAXIMUM_FRAMES { type = .last }
                    currentFrame = Frame(type: type)
                }
            }
            return true
        }
        return false
    }
    
    private func hasGameEnded() -> Bool { return (completedFrames.count == MAXIMUM_FRAMES) }
    
    private mutating func notifyScore() {
        score = calculateScore()
        didEndGame?(score)
    }
    
    private func calculateScore() -> Int {
        var score = 0
        
        // Normally calculating score for all frames
        for frame in completedFrames {
            let firstRollPinFalls = frame.firstRollPinFalls ?? 0
            let secondRollPinFalls = frame.secondRollPinFalls ?? 0
            let thirdRollPinFalls = frame.thirdRollPinFalls ?? 0
            score += firstRollPinFalls + secondRollPinFalls + thirdRollPinFalls
        }
        
        // Calculating bonus points for all strikes
        for (index, frame) in completedFrames.enumerated() {
            guard frame.isStrike() else { continue }
            
            if index + 1 < completedFrames.count {
                let nextFrame = completedFrames[index + 1]
                
                score += nextFrame.firstRollPinFalls ?? 0
                score += nextFrame.secondRollPinFalls ?? 0
                
                if nextFrame.isStrike(), index + 2 < completedFrames.count {
                    // Check the next frame
                    let nextNextFrame = completedFrames[index + 2]
                    score += nextNextFrame.firstRollPinFalls ?? 0
                }
            }
        }
        
        // Calculating bonus points for all spares
        for (index, frame) in completedFrames.enumerated() {
            guard frame.isSpare() else { continue }
            
            if index + 1 < completedFrames.count {
                let nextFrame = completedFrames[index + 1]
                let firstRollPinFalls = nextFrame.firstRollPinFalls ?? 0
                score += firstRollPinFalls
            }
        }
        
        return score
    }
}
