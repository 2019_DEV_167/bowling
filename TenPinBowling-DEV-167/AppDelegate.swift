//
//  AppDelegate.swift
//  TenPinBowling-DEV-167
//
//  Created by DEV-167 on 20/05/19.
//  Copyright © 2019 DEV-167. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

