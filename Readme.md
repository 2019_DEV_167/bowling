- Open TenPinBowling-DEV-167.xcodeproj
- Goto TenPinBowling-DEV-167Tests target
- Open TenPinBowling_DEV_167Tests file
- Press Cmd+U to run all the tests. Alternatively you can run each test by clicking the little play icon on the left side of the name of the test.


There are different tests for different inputs. They do not expose any implementation details of the Game class, yet test coverage is 95.5%