//
//  TenPinBowling_DEV_167Tests.swift
//  TenPinBowling-DEV-167Tests
//
//  Created by DEV-167 on 20/05/19.
//  Copyright © 2019 DEV-167. All rights reserved.
//

import XCTest
@testable import TenPinBowling_DEV_167

class TenPinBowling_DEV_167Tests: XCTestCase {
    
    var subjectUnderTest: TenPinBowlingGame!
    
    override func setUp() {
        subjectUnderTest = TenPinBowlingGame()
    }
    
    override func tearDown() {
        subjectUnderTest = nil
    }
    
    func testGameInitialization() { XCTAssertNotNil(subjectUnderTest.currentFrame) }
    
    func test_52_43_61_44_40_18_26_32_53_44_Score() {
        
        let gameEndExpectation = expectation(description: "Game end expectation")
        
        subjectUnderTest.didEndGame = { score in
            XCTAssertEqual(score, 71)
            gameEndExpectation.fulfill()
        }
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 2))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 3))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 6))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 1))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 1))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 8))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 2))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 6))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 3))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 2))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 3))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        
        // Game over. Now record function should throw false
        XCTAssertFalse(subjectUnderTest.record(pinsFell: 3))
        XCTAssertFalse(subjectUnderTest.record(pinsFell: 2))
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_Reject_Score_Record_After_Game_End() {
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10)) // 3
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10)) // 6
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10)) // 9
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10)) // 12
        
        // Game over. Now record function should throw false
        XCTAssertFalse(subjectUnderTest.record(pinsFell: 3))
        XCTAssertFalse(subjectUnderTest.record(pinsFell: 2))
    }
    
    func test_X_X_X_X_X_X_X_X_X_X_XX_PerfectGameScore() {
        
        let gameEndExpectation = expectation(description: "Game end expectation")
        
        subjectUnderTest.didEndGame = { score in
            XCTAssertEqual(score, 300)
            gameEndExpectation.fulfill()
        }
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10)) // 3
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10)) // 6
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10)) // 9
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10)) // 12
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_90_90_90_90_90_90_90_90_90_90_GameScore() {
        
        let gameEndExpectation = expectation(description: "Game end expectation")
        
        subjectUnderTest.didEndGame = { score in
            XCTAssertEqual(score, 90)
            gameEndExpectation.fulfill()
        }
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 9))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 9))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 9))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0)) // 3
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 9))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 9))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 9))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0)) // 6
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 9))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 9))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 9))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0)) // 9
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 9))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0)) // 10
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_55_55_55_55_55_55_55_55_555_AllSpares_GameScore() {
        
        let gameEndExpectation = expectation(description: "Game end expectation")
        
        subjectUnderTest.didEndGame = { score in
            XCTAssertEqual(score, 150)
            gameEndExpectation.fulfill()
        }
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5)) // 3
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5)) // 6
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5)) // 9
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5)) // 10
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_52_43_61_44_46_18_26_32_X_44_Score() {
        
        let gameEndExpectation = expectation(description: "Game end expectation")
        
        subjectUnderTest.didEndGame = { score in
            XCTAssertEqual(score, 88)
            gameEndExpectation.fulfill()
        }
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 2))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 3))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 6))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 1))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 6))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 1))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 8))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 2))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 6))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 3))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 2))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_00_00_00_00_00_00_00_00_00_00_Score() {
        
        let gameEndExpectation = expectation(description: "Game end expectation")
        
        subjectUnderTest.didEndGame = { score in
            XCTAssertEqual(score, 0)
            gameEndExpectation.fulfill()
        }
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_45_54_36_27_09_63_81_18_90_73_5_Score() {
        
        let gameEndExpectation = expectation(description: "Game end expectation")
        
        subjectUnderTest.didEndGame = { score in
            XCTAssertEqual(score, 96)
            gameEndExpectation.fulfill()
        }
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 3))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 6))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 2))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 7))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 9))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 6))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 3))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 8))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 1))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 1))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 8))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 9))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 7))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 3))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 5))
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_12_34_010_010_X_X_00_18_72_X_010_Score() {
        
        let gameEndExpectation = expectation(description: "Game end expectation")
        
        subjectUnderTest.didEndGame = { score in
            XCTAssertEqual(score, 108)
            gameEndExpectation.fulfill()
        }
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 1))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 2))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 3))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 4))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 1))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 8))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 7))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 2))
        
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 0))
        XCTAssertTrue(subjectUnderTest.record(pinsFell: 10))
        
        waitForExpectations(timeout: 1, handler: nil)
    }
}
